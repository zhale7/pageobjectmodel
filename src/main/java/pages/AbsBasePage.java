package pages;

import org.openqa.selenium.WebDriver;
import pageobject.AbsPageObect;

public abstract class AbsBasePage extends AbsPageObect {
    private final static String BASE_URL = System.getProperty("base.url");

    public AbsBasePage(WebDriver driver) {
        super(driver);
    }

    public void open(String path) {
        driver.get(BASE_URL + path);
    }

}
