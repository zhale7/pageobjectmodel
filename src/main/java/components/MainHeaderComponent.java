package components;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.ConcertPage;

public class MainHeaderComponent extends AbsBaseComponent{
    public MainHeaderComponent(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "span>a[href=\"/en/events/concerts\"]")
    private WebElement concert;

    public ConcertPage chooseConcert(){
        concert.click();
        return new ConcertPage(driver);
    }
}
