package components;

import org.openqa.selenium.WebDriver;
import pageobject.AbsPageObect;

public abstract class AbsBaseComponent extends AbsPageObect {

    public AbsBaseComponent(WebDriver driver) {
        super(driver);
    }
}
